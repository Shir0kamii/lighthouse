use std::{num::ParseIntError, str::FromStr};

use thiserror::Error;

use super::house::House;
use crate::cell_position::CellPosition;

/// Represents a house of a certain size.
#[derive(Clone, Copy, Debug)]
pub struct HouseSetup {
    /// The width of the house.
    x: usize,

    /// The height of the house.
    y: usize,
}

impl HouseSetup {
    /// Create a new house setup with the given size.
    #[must_use]
    pub const fn new(x: usize, y: usize) -> Self {
        Self { x, y }
    }

    /// Build a house instance from the setup.
    #[must_use]
    pub fn build(self) -> House {
        House::new(self)
    }

    /// Test a solution on the setup.
    #[must_use]
    pub fn test_solution<I>(&self, solution: I) -> bool
    where
        I: IntoIterator<Item = CellPosition>,
    {
        let mut house = self.build();
        solution.into_iter().for_each(|pos| house.play_move(pos));
        house.verify()
    }
}

/// Accessors
impl HouseSetup {
    /// Get the width.
    #[must_use]
    pub const fn x(&self) -> usize {
        self.x
    }

    /// Get the height.
    #[must_use]
    pub const fn y(&self) -> usize {
        self.y
    }
}

#[derive(Debug, Error)]
pub enum InvalidSetup {
    #[error("no separator 'x' found")]
    NoSeparator,
    #[error("couldn't parse number: {0}")]
    InvalidNumber(#[from] ParseIntError),
}

impl FromStr for HouseSetup {
    type Err = InvalidSetup;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (x, y) = s.split_once('x').ok_or(InvalidSetup::NoSeparator)?;
        let x: usize = x.parse()?;
        let y: usize = y.parse()?;
        Ok(Self::new(x, y))
    }
}
