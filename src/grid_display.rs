use std::{fmt, iter};

use crate::cell_set::CellSet;
use crate::house_setup::HouseSetup;

pub struct GridDisplay<'a> {
    cells: &'a CellSet,
    setup: &'a HouseSetup,
}

impl<'a> GridDisplay<'a> {
    pub const fn new(cells: &'a CellSet, setup: &'a HouseSetup) -> Self {
        Self { cells, setup }
    }

    fn line(&self, y: usize) -> String {
        (0..self.setup.x())
            .map(|x| {
                if self.cells.contains(&(x, y).into()) {
                    "x"
                } else {
                    " "
                }
            })
            .intersperse("│")
            .collect()
    }

    fn line_separator(&self) -> String {
        iter::repeat("─".to_owned())
            .intersperse("┼".to_owned())
            .take(2 * self.setup.x() - 1)
            .collect()
    }

    fn grid(&self) -> String {
        (0..self.setup.y())
            .map(|y| self.line(y))
            .intersperse(self.line_separator())
            .intersperse("\n".to_owned())
            .collect()
    }
}

impl<'a> fmt::Display for GridDisplay<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.grid())
    }
}
