use crate::cell_set::CellSet;

pub type SolutionIterator = Box<dyn Iterator<Item = CellSet>>;
