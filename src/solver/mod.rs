//! Algorithms to solve the lighthouse problem.
//!
//! All algorithms should return all solutions in any order.

mod first_line;
mod naive;

use clap::ValueEnum;

use first_line::Solver as FirstLineSolver;
use naive::Solver as NaiveSolver;

use crate::house_setup::HouseSetup;
use crate::solutions::SolutionIterator;

/// The different solver algorithms.
#[derive(Debug, Default, Clone, ValueEnum)]
pub enum Algorithm {
    /// An algorithm that tries all possible combinations of moves.
    Naive,

    /// An algorithm that tries all combinations for the first line, then build the rest of the
    /// solution from that first line.
    #[default]
    FirstLine,
}

impl Algorithm {
    /// Solve a house setup using the selected algorithm.
    #[must_use]
    pub fn solve(&self, setup: HouseSetup) -> SolutionIterator {
        match self {
            Self::Naive => NaiveSolver::new(setup).run(),
            Self::FirstLine => FirstLineSolver::new(setup).run(),
        }
    }
}
