use itertools::Itertools;

use crate::cell_position::CellPosition;
use crate::cell_set::CellSet;
use crate::house_setup::HouseSetup;
use crate::solutions::SolutionIterator;

/// Solves a given setup by testing all possible solutions.
#[derive(Clone, Debug)]
pub struct Solver {
    /// The house setup the solver is set for.
    setup: HouseSetup,
}

impl Solver {
    /// Instanciate the solver with a given house setup.
    #[must_use]
    pub const fn new(setup: HouseSetup) -> Self {
        Self { setup }
    }

    /// Get a list of all single moves possible.
    fn get_possible_moves(&self) -> Vec<CellPosition> {
        let x_range = 0..self.setup.x();
        let y_range = 0..self.setup.y();
        x_range
            .cartesian_product(y_range)
            .map(std::convert::Into::into)
            .collect()
    }

    /// Run the algorithm on the given house setup and returns a list of all solutions.
    #[must_use]
    pub fn run(self) -> SolutionIterator {
        let total_size = self.setup.x() * self.setup.y();
        let minimal_nb_moves = total_size.div_ceil(5);
        let possible_moves = self.get_possible_moves();
        let solution_iterator = (minimal_nb_moves..=total_size).flat_map(move |size| {
            possible_moves
                .clone()
                .into_iter()
                .combinations(size)
                .map(CellSet::from_iter)
                .filter(move |moves| self.setup.test_solution(moves.clone()))
        });
        Box::new(solution_iterator)
    }
}
