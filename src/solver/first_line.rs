use itertools::Itertools;

use crate::cell_set::CellSet;
use crate::house_setup::HouseSetup;
use crate::solutions::SolutionIterator;

/// Solves a given setup by trying all possible first lines, then building the following lines by
/// triggering the cells below the inactivated cells of the preceding line.
#[derive(Clone, Debug)]
pub struct Solver {
    /// The house setup the solver is set for.
    setup: HouseSetup,
}

impl Solver {
    /// Instanciate the solver with a given house setup.
    #[must_use]
    pub const fn new(setup: HouseSetup) -> Self {
        Self { setup }
    }

    /// Test if a set of moves for the first line lead to a valid solution, building said solution
    /// in the process.
    ///
    /// Returns `None` if the given first line lead to an invalid solution.
    fn test_first_line(&self, first_line_moves: CellSet) -> Option<CellSet> {
        let mut house = self.setup.build();
        let mut current_moves = first_line_moves;
        let mut move_list = current_moves.clone();
        for row in 0..(self.setup.y() - 1) {
            current_moves
                .into_iter()
                .for_each(|cell| house.play_move(cell));
            current_moves = house
                .row(row)
                .unwrap()
                .iter()
                .enumerate()
                .filter_map(|(i, state)| (!state).then_some((i, row + 1).into()))
                .collect::<CellSet>();
            move_list.extend(current_moves.clone());
        }
        current_moves
            .into_iter()
            .for_each(|cell| house.play_move(cell));
        house.verify().then_some(move_list)
    }

    /// Build an iterator of all move combinations for the first line.
    fn get_first_line_combinations(&self) -> impl Iterator<Item = CellSet> {
        let moves_first_line = (0..self.setup.x())
            .map(|x| (x, 0).into())
            .collect::<CellSet>();
        (0..=self.setup.x()).flat_map(move |nb_moves| {
            moves_first_line
                .clone()
                .into_iter()
                .combinations(nb_moves)
                .map(CellSet::from_iter)
        })
    }

    /// Run the algorithm on the given house setup and returns a list of all solutions.
    #[must_use]
    pub fn run(self) -> SolutionIterator {
        let solution_iterator = self
            .get_first_line_combinations()
            .filter_map(move |moves| self.test_first_line(moves));
        Box::new(solution_iterator)
    }
}
