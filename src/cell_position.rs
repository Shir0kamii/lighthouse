use std::fmt;

use crate::house_setup::HouseSetup;

/// Represents the position of a cell.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct CellPosition {
    x: usize,
    y: usize,
}

impl From<(usize, usize)> for CellPosition {
    fn from(position: (usize, usize)) -> Self {
        Self::new(position.0, position.1)
    }
}

impl From<CellPosition> for (usize, usize) {
    fn from(position: CellPosition) -> (usize, usize) {
        (position.x(), position.y())
    }
}

impl fmt::Display for CellPosition {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}x{}", self.x, self.y)
    }
}

impl CellPosition {
    /// Create a move from given x & y coordinates.
    #[must_use]
    pub const fn new(x: usize, y: usize) -> Self {
        Self { x, y }
    }

    /// Getter for x coordinate.
    #[must_use]
    pub const fn x(&self) -> usize {
        self.x
    }

    /// Getter for y coordinate.
    #[must_use]
    pub const fn y(&self) -> usize {
        self.y
    }
}

/// Implementation of mirroring and rotations.
impl CellPosition {
    pub const fn mirror_horizontally(&self, setup: &HouseSetup) -> Self {
        Self::new(setup.x() - self.x - 1, self.y)
    }

    pub const fn mirror_vertically(&self, setup: &HouseSetup) -> Self {
        Self::new(self.x, setup.y() - self.y - 1)
    }

    pub const fn rotate_90(&self, setup: &HouseSetup) -> Self {
        Self::new(setup.x() - self.y - 1, self.x)
    }

    pub const fn rotate_180(&self, setup: &HouseSetup) -> Self {
        Self::new(setup.x() - self.x - 1, setup.y() - self.y - 1)
    }

    pub const fn rotate_270(&self, setup: &HouseSetup) -> Self {
        Self::new(self.y, setup.y() - self.x - 1)
    }
}

/// Getter for triggered cells.
impl CellPosition {
    /// Get the cell above the triggered one.
    #[must_use]
    pub fn up(&self) -> Option<Self> {
        if self.y == 0 {
            return None;
        }

        Some((self.x, self.y - 1).into())
    }

    /// Get the cell to the right of the triggered one.
    #[allow(clippy::unnecessary_wraps)]
    #[must_use]
    pub fn right(&self) -> Option<Self> {
        Some((self.x + 1, self.y).into())
    }

    /// Get the cell below the triggered one.
    #[allow(clippy::unnecessary_wraps)]
    #[must_use]
    pub fn down(&self) -> Option<Self> {
        Some((self.x, self.y + 1).into())
    }

    /// Get the cell to the left of the triggered one.
    #[must_use]
    pub fn left(&self) -> Option<Self> {
        if self.x == 0 {
            return None;
        }

        Some((self.x - 1, self.y).into())
    }

    /// Get the list of cell toggled by triggering this cell.
    #[must_use]
    pub fn toggled(&self) -> Vec<Self> {
        vec![
            Some(*self),
            self.up(),
            self.right(),
            self.down(),
            self.left(),
        ]
        .into_iter()
        .flatten()
        .collect()
    }
}
