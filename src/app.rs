use std::collections::HashSet;

use clap::Parser;
use itertools::Itertools;

use super::cell_set::CellSet;
use super::house_setup::HouseSetup;
use super::solutions::SolutionIterator;
use super::solver::Algorithm;

/// Get the solutions for a lighthouse of a certain size.
#[allow(clippy::struct_excessive_bools)]
#[derive(Debug, Clone, Parser)]
#[command(version)]
pub struct App {
    /// The setup of the lighthouse to solve. (eg. "5x5")
    setup: HouseSetup,

    /// The algorithm used to solve the lighthouse.
    #[arg(long, short, value_enum, default_value_t)]
    algorithm: Algorithm,

    /// Print the solutions as a list of cells.
    #[arg(long, short)]
    list_cells: bool,

    /// Show similar solutions.
    #[arg(long, short = 'S')]
    show_similars: bool,

    /// Sort solutions.
    #[arg(long, short)]
    sorted: bool,

    /// Get a single solution
    #[arg(long, short = '1', conflicts_with_all = ["show_similars", "sorted"])]
    single: bool,
}

impl App {
    /// Return a list of solutions excluding similar solutions.
    fn filter_out_similar_solutions(&self, solutions: SolutionIterator) -> SolutionIterator {
        let setup = self.setup;
        let mut seen: HashSet<CellSet> = HashSet::new();
        let solutions = solutions.filter(move |solution| {
            let new_solution = !seen.contains(solution);
            if new_solution {
                seen.extend(solution.similar_sets(&setup));
            }
            new_solution
        });
        Box::new(solutions)
    }

    fn apply_args_to_solutions(&self, mut solutions: SolutionIterator) -> SolutionIterator {
        if !self.show_similars {
            solutions = self.filter_out_similar_solutions(solutions);
        }
        if self.sorted {
            solutions = Box::new(solutions.sorted());
        }
        solutions
    }

    /// Get the solutions to the specified house setup.
    fn get_solutions(&self) -> SolutionIterator {
        self.algorithm.solve(self.setup)
    }

    /// Print solutions as specified by options.
    fn print_all_solutions(&self, solutions: SolutionIterator) {
        let mut nb_solutions = 0;
        for solution in solutions {
            nb_solutions += 1;
            self.print_single_solution(&solution, nb_solutions);
        }
        if nb_solutions > 0 {
            println!("Number of solutions: {nb_solutions}");
        } else {
            println!("No solution");
        }
    }

    fn print_single_solution(&self, solution: &CellSet, nb_solution: usize) {
        if self.list_cells {
            println!(
                "Solution {nb_solution} ({} moves): {solution}",
                solution.len()
            );
        } else {
            println!(
                "Solution {nb_solution} ({} moves):\n{}",
                solution.len(),
                solution.grid(&self.setup)
            );
        }
    }

    /// Main code of the application.
    pub fn run(self) {
        let mut solution_list = self.get_solutions();
        if self.single {
            if let Some(solution) = solution_list.next() {
                self.print_single_solution(&solution, 1);
            } else {
                println!("No solution");
            }
        } else {
            let solution_list = self.apply_args_to_solutions(solution_list);
            self.print_all_solutions(solution_list);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use clap::CommandFactory;

    #[test]
    fn debug_assert() {
        App::command().debug_assert();
    }
}
