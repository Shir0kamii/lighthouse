use std::{
    cmp::Ordering,
    collections::{
        btree_set::{BTreeSet, IntoIter, Iter},
        HashSet,
    },
    fmt,
    hash::Hash,
};

use itertools::join;

use crate::cell_position::CellPosition;
use crate::grid_display::GridDisplay;
use crate::house_setup::HouseSetup;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct CellSet {
    set: BTreeSet<CellPosition>,
}

/// Implementation of `BTreeSet` methods.
impl CellSet {
    pub fn len(&self) -> usize {
        self.set.len()
    }

    pub fn iter(&self) -> Iter<CellPosition> {
        self.set.iter()
    }

    pub fn contains(&self, value: &CellPosition) -> bool {
        self.set.contains(value)
    }
}

/// Implementation of mirroring and rotation methods.
impl CellSet {
    pub fn mirror_horizontally(&self, setup: &HouseSetup) -> Self {
        self.iter()
            .map(|cell| cell.mirror_horizontally(setup))
            .collect()
    }

    pub fn mirror_vertically(&self, setup: &HouseSetup) -> Self {
        self.iter()
            .map(|cell| cell.mirror_vertically(setup))
            .collect()
    }

    pub fn rotate_90(&self, setup: &HouseSetup) -> Self {
        self.iter().map(|cell| cell.rotate_90(setup)).collect()
    }

    pub fn rotate_180(&self, setup: &HouseSetup) -> Self {
        self.iter().map(|cell| cell.rotate_180(setup)).collect()
    }

    pub fn rotate_270(&self, setup: &HouseSetup) -> Self {
        self.iter().map(|cell| cell.rotate_270(setup)).collect()
    }
}

/// Implementation of original methods.
impl CellSet {
    pub const fn grid<'a>(&'a self, setup: &'a HouseSetup) -> GridDisplay<'a> {
        GridDisplay::new(self, setup)
    }

    pub fn similar_sets(&self, setup: &HouseSetup) -> HashSet<Self> {
        let mut similars: HashSet<Self> = HashSet::new();
        similars.insert(self.clone());
        similars.insert(self.mirror_horizontally(setup));
        similars.insert(self.mirror_vertically(setup));
        similars.insert(self.rotate_180(setup));
        if setup.x() == setup.y() {
            similars.insert(self.rotate_90(setup));
            similars.insert(self.rotate_270(setup));
        }
        similars
    }
}

impl fmt::Display for CellSet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", join(self.iter(), ", "))
    }
}

impl FromIterator<CellPosition> for CellSet {
    fn from_iter<T>(iter: T) -> Self
    where
        T: IntoIterator<Item = CellPosition>,
    {
        Self {
            set: BTreeSet::from_iter(iter),
        }
    }
}

impl IntoIterator for CellSet {
    type Item = CellPosition;
    type IntoIter = IntoIter<CellPosition>;

    fn into_iter(self) -> Self::IntoIter {
        self.set.into_iter()
    }
}

impl Extend<CellPosition> for CellSet {
    fn extend<T>(&mut self, iter: T)
    where
        T: IntoIterator<Item = CellPosition>,
    {
        self.set.extend(iter);
    }
}

impl PartialOrd for CellSet {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for CellSet {
    fn cmp(&self, other: &Self) -> Ordering {
        self.len().cmp(&other.len())
    }
}
