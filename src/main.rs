#![feature(int_roundings)]
#![feature(iter_intersperse)]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![allow(clippy::option_if_let_else)]

use clap::Parser;

mod app;
mod cell_position;
mod cell_set;
mod grid_display;
mod house;
mod house_setup;
mod solutions;
mod solver;

use app::App;

/// Entry point of the program.
fn main() {
    App::parse().run();
}
