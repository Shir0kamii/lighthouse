use grid::Grid;

use super::house_setup::HouseSetup;
use crate::cell_position::CellPosition;

/// Represents a house with its state.
#[derive(Debug, Clone)]
pub struct House {
    states: Grid<bool>,
}

impl House {
    /// Toggle the state of a cell if it exist.
    ///
    /// Returns true if a cell's state was triggered, or false if it doesn't exist.
    fn toggle_state(&mut self, cell: CellPosition) -> bool {
        if let Some(state) = self.states.get_mut(cell.y(), cell.x()) {
            *state = !*state;
            true
        } else {
            false
        }
    }

    /// Create a new house with inactive cells from a given setup.
    #[must_use]
    pub fn new(setup: HouseSetup) -> Self {
        Self {
            states: Grid::new(setup.y(), setup.x()),
        }
    }

    /// Trigger a cell and its horizontal/vertical siblings.
    pub fn play_move(&mut self, cell: CellPosition) {
        for toggled_cell in cell.toggled() {
            self.toggle_state(toggled_cell);
        }
    }

    /// Get a row of cells if it exists.
    #[must_use]
    pub fn row(&self, y: usize) -> Option<&[bool]> {
        if y >= self.states.rows() {
            None
        } else {
            Some(&self.states[y])
        }
    }

    /// Verify that all cells are activated.
    #[must_use]
    pub fn verify(&self) -> bool {
        self.states.iter().all(|state| *state)
    }
}
